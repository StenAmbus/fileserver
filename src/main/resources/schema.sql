CREATE TABLE IF NOT EXISTS directory (
  id            IDENTITY     NOT NULL,
  parent_dir_id BIGINT,
  name          VARCHAR(255) NOT NULL,
  creation_date    DATE         NOT NULL,

  CONSTRAINT directory_pk PRIMARY KEY (id),
  FOREIGN KEY (parent_dir_id) REFERENCES directory (id)
);

CREATE TABLE IF NOT EXISTS file (
  id               IDENTITY     NOT NULL,
  parent_dir_id    BIGINT       NOT NULL,
  name             VARCHAR(255) NOT NULL,
  creation_date       DATE         NOT NULL,
  last_accessed_date  DATE,
  size             BIGINT       NOT NULL,

  CONSTRAINT file_pk PRIMARY KEY (id),
  FOREIGN KEY (parent_dir_id) REFERENCES directory (id)
);
