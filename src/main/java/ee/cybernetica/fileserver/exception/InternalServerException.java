package ee.cybernetica.fileserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends RuntimeException{

    public InternalServerException() {
        super("Ohh noo... something is wrong with the server.");
    }
}
