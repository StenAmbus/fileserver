package ee.cybernetica.fileserver.mapper;

import ee.cybernetica.fileserver.dto.FileServerObjectDTO;
import ee.cybernetica.fileserver.model.Directory;
import ee.cybernetica.fileserver.model.File;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FileServerObjectDTOMapper {

    @Mapping(target = "object", constant = "Directory")
    @Mapping(target = "lastAccessDate", ignore = true)
    @Mapping(target = "size", ignore = true)
    FileServerObjectDTO map(Directory directory);

    @Mapping(target = "object", constant = "File")
    FileServerObjectDTO map(File file);

}
