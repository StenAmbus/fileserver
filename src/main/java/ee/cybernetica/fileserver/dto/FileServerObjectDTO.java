package ee.cybernetica.fileserver.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class FileServerObjectDTO {
    private Long id;
    private String object;
    private String name;
    private LocalDate creationDate;
    private LocalDate lastAccessDate;
    private Long size;
}
