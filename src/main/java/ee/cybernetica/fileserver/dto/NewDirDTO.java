package ee.cybernetica.fileserver.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class NewDirDTO {
    @NotNull
    private String name;
}
