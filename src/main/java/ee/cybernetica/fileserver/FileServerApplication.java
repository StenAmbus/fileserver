package ee.cybernetica.fileserver;

import ee.cybernetica.fileserver.exception.InternalServerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
public class FileServerApplication {

	public static void main(String[] args){
		SpringApplication.run(FileServerApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void createFilesFolder() {
		try {
			Path serverFilePath = Paths.get("files/");
			Files.createDirectories(serverFilePath);
		} catch (IOException e) {
			throw new InternalServerException();
		}
	}
}
