package ee.cybernetica.fileserver.service;

import ee.cybernetica.fileserver.dto.FileServerObjectDTO;
import ee.cybernetica.fileserver.dto.NewDirDTO;
import ee.cybernetica.fileserver.exception.ResourceNotFoundException;
import ee.cybernetica.fileserver.mapper.FileServerObjectDTOMapper;
import ee.cybernetica.fileserver.model.Directory;
import ee.cybernetica.fileserver.repository.DirectoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DirectoryService {

    private final DirectoryRepository directoryRepository;
    private final FileServerObjectDTOMapper fileServerObjectDTOMapper;
    private final FileService fileService;

    public void addNewTopLevelDir(NewDirDTO newDirDTO) {
        Directory directory = new Directory()
                .setName(newDirDTO.getName())
                .setCreationDate(LocalDate.now());
        directoryRepository.save(directory);
    }

    public void addNewDir(NewDirDTO newDirDTO, Long dirId) {
        Directory parentDirectory = getDirectoryById(dirId);
        Directory directory = new Directory()
                .setName(newDirDTO.getName())
                .setParentDirectory(parentDirectory)
                .setCreationDate(LocalDate.now());
        directoryRepository.save(directory);
    }

    public List<FileServerObjectDTO> getDirectoriesInTopLevel() {
        return directoryRepository.findByParentDirectoryIsNull()
                .stream()
                .map(fileServerObjectDTOMapper::map)
                .collect(Collectors.toList());
    }

    public Directory getDirectoryById(Long dirId){
        return directoryRepository.findById(dirId)
                .orElseThrow(() -> new ResourceNotFoundException("No directory found with given ID"));
    }

    public List<FileServerObjectDTO> getDirectoriesById(Long dirId) {
        return directoryRepository.findByParentDirectoryId(dirId)
                .stream()
                .map(fileServerObjectDTOMapper::map)
                .collect(Collectors.toList());
    }

    public boolean directoryExistsById(Long dirId){
        return directoryRepository.existsById(dirId);
    }

    public boolean isDirectoryEmpty(Long dirId){
        return directoryRepository.findByParentDirectoryId(dirId).isEmpty() && fileService.isDirectoryEmpty(dirId);
    }

    public void deleteDirectory(Long dirId) {
        Directory directory = getDirectoryById(dirId);
        if(!isDirectoryEmpty(dirId))
            throw new ResourceNotFoundException("This directory is not empty");
        directoryRepository.delete(directory);
    }
}
