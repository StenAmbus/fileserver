package ee.cybernetica.fileserver.service;

import ee.cybernetica.fileserver.dto.FileServerObjectDTO;
import ee.cybernetica.fileserver.exception.InternalServerException;
import ee.cybernetica.fileserver.exception.ResourceNotFoundException;
import ee.cybernetica.fileserver.mapper.FileServerObjectDTOMapper;
import ee.cybernetica.fileserver.model.Directory;
import ee.cybernetica.fileserver.model.File;
import ee.cybernetica.fileserver.repository.FileRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FileService {

    private final FileRepository fileRepo;
    private final FileServerObjectDTOMapper fileServerObjectDTOMapper;

    public ByteArrayResource getFileResourceById(Long id){
        if(!fileRepo.existsById(id))
            throw new ResourceNotFoundException("No file found with given ID");

        try{
            Path serverFilePath = Paths.get("files\\" + id.toString());

            byte[] fileContent = Files.readAllBytes(serverFilePath);
            ByteArrayResource downloadableFile = new ByteArrayResource(fileContent);

            fileRepo.updateLastAccessDate(id, LocalDate.now());

            return downloadableFile;
        } catch (IOException e){
            throw new InternalServerException();
        }
    }

    public File getFileById(Long id) {
        return fileRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No file found with given ID"));
    }

    public List<FileServerObjectDTO> getFilesInTopLevel() {
        return fileRepo.findByParentDirectoryIsNull()
                .stream()
                .map(fileServerObjectDTOMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    public void addFile(MultipartFile multipartFile, Directory directory){
        try {
            File newFile = new File()
                    .setName(multipartFile.getOriginalFilename())
                    .setParentDirectory(directory)
                    .setCreationDate(LocalDate.now())
                    .setSize(multipartFile.getSize());
            newFile = fileRepo.save(newFile);
            multipartFile.transferTo(Files.createFile(Paths.get("files\\" + newFile.getId().toString())));
        } catch (IOException e) {
            throw new InternalServerException();
        }
    }

    public List<FileServerObjectDTO> getFilesByDirId(Long dirId) {
        return fileRepo.findByParentDirectoryId(dirId)
                .stream()
                .map(fileServerObjectDTOMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteFile(Long fileId) {
        File deletableFile = getFileById(fileId);
        try {
            fileRepo.delete(deletableFile);
            Path serverFilePath = Paths.get("files/" + fileId.toString());
            Files.delete(serverFilePath);
        } catch (IOException e) {
            throw new InternalServerException();
        }
    }

    public boolean isDirectoryEmpty(Long dirId) {
        return fileRepo.findByParentDirectoryId(dirId).isEmpty();
    }
}