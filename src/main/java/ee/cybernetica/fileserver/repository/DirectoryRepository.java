package ee.cybernetica.fileserver.repository;

import ee.cybernetica.fileserver.model.Directory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DirectoryRepository extends JpaRepository<Directory, Long> {

    List<Directory> findByParentDirectoryIsNull();

    List<Directory> findByParentDirectoryId(Long id);

    boolean existsById(Long id);
}
