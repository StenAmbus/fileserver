package ee.cybernetica.fileserver.repository;

import ee.cybernetica.fileserver.model.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

    List<File> findByParentDirectoryId(Long id);

    List<File> findByParentDirectoryIsNull();

    @Transactional
    @Modifying
    @Query("UPDATE File f SET f.lastAccessDate = :newLastAccessDate WHERE f.id= :id")
    void updateLastAccessDate(@Param("id") Long id, @Param("newLastAccessDate") LocalDate newLastAccessDate);
}
