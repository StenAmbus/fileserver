package ee.cybernetica.fileserver.controller;

import ee.cybernetica.fileserver.dto.FileServerObjectDTO;
import ee.cybernetica.fileserver.dto.NewDirDTO;
import ee.cybernetica.fileserver.exception.ResourceNotFoundException;
import ee.cybernetica.fileserver.service.DirectoryService;
import ee.cybernetica.fileserver.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class FileServerController {

    private final FileService fileService;
    private final DirectoryService directoryService;

    @GetMapping("/dir")
    public ResponseEntity<List<FileServerObjectDTO>> getDirectoryData(){
        List<FileServerObjectDTO> directories = directoryService.getDirectoriesInTopLevel();
        List<FileServerObjectDTO> files = fileService.getFilesInTopLevel();
        List<FileServerObjectDTO> fileServerObjectDTOs = new ArrayList<>();
        fileServerObjectDTOs.addAll(directories);
        fileServerObjectDTOs.addAll(files);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fileServerObjectDTOs);
    }

    @GetMapping("/dir/{dirId}")
    public ResponseEntity<List<FileServerObjectDTO>> getDirectoryDataById(@PathVariable Long dirId){
        if(!directoryService.directoryExistsById(dirId))
            throw new ResourceNotFoundException("No directory found with given ID");
        List<FileServerObjectDTO> directories = directoryService.getDirectoriesById(dirId);
        List<FileServerObjectDTO> files = fileService.getFilesByDirId(dirId);
        List<FileServerObjectDTO> fileServerObjectDTOs = new ArrayList<>();
        fileServerObjectDTOs.addAll(directories);
        fileServerObjectDTOs.addAll(files);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(fileServerObjectDTOs);
    }

    @GetMapping("/file/{id}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable(name = "id") Long id){
        ByteArrayResource resource = fileService.getFileResourceById(id);
        String filename = fileService.getFileById(id).getName();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(resource.contentLength())
                .header(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition.attachment().filename(filename).build().toString())
                .body(resource);
    }


    @PostMapping("/dir")
    public ResponseEntity<String> createDir(@Valid @RequestBody NewDirDTO newDirDTO){
        directoryService.addNewTopLevelDir(newDirDTO);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.TEXT_PLAIN)
                .body("Directory created");
    }

    @PostMapping("/dir/{dirId}")
    public ResponseEntity<String> createDirById(@Valid @RequestBody NewDirDTO newDirDTO, @PathVariable Long dirId){
        directoryService.addNewDir(newDirDTO, dirId);
        return ResponseEntity.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body("Directory created");
    }

    @PostMapping("/dir/{dirId}/file")
    public ResponseEntity<String> createFileById(@RequestParam("file") MultipartFile multipartFile, @PathVariable Long dirId){
        fileService.addFile(multipartFile, directoryService.getDirectoryById(dirId));
        return ResponseEntity.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body("File created");
    }


    @DeleteMapping("/dir/{dirId}")
    public ResponseEntity<String> deleteDirectoryById(@PathVariable Long dirId){
        directoryService.deleteDirectory(dirId);
        return ResponseEntity.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body("Directory deleted");
    }

    @DeleteMapping("/file/{fileId}")
    public ResponseEntity<String> deleteFileById(@PathVariable Long fileId){
        fileService.deleteFile(fileId);
        return ResponseEntity.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body("File deleted");
    }
}
